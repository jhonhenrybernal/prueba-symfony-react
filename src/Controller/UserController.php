<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * @Route("api", name="api_")
 */
class UserController extends AbstractFOSRestController
{

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->view($this->userRepository->findAll(), Response::HTTP_OK);
    }

     /**
     * @Route("/register", name="register")
     */
    public function register(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');
        $createdById = $request->get('createdById');

        $user = $this->userRepository->findOneBy([
            'username' => $username,
        ]);

        if (!is_null($user)) {
            return $this->view([
                'message' => 'Usuario ya existe'
            ], Response::HTTP_CONFLICT);
        }

        $user = new User();

        $user->setUsername($username);
        $user->setRoles($user->GetRoles());
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $password)
        );
        $user->setCreatedById($createdById);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->view([ 'message' => 'Usuario creado'], Response::HTTP_CREATED);
    }
}
